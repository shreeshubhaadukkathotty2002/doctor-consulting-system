/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.doctorconsultingsystem.ui.model.data;

/**
 *
 * @author student
 */
public class PatientDetail {
    private Integer Id;
    private String patientName;
    private String patientDesease;
    private Integer doctorId;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientDesease() {
        return patientDesease;
    }

    public void setPatientDesease(String patientDesease) {
        this.patientDesease = patientDesease;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }
    
public PatientDetail(Integer Id,String patientName,String patientDesease,Integer doctorId){
    this.patientName = patientName;
    this.patientDesease = patientDesease;
    this.doctorId = doctorId;
}
}
