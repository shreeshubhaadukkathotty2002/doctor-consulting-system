/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.doctorconsultingsystem.ui.model.data;

/**
 *
 * @author student
 */
public class DoctorDetail {
    private Integer id;
    private String doctorname;
    private String specialisation;
    private Integer roomno;
    private Float doctorfees;

    public DoctorDetail(Integer id,String doctorname,String specialisation,Integer roomno,Float doctorfees) {
        this.id = id;
        this.doctorname = doctorname;
        this.specialisation = specialisation;
        this.roomno = roomno;
        this.doctorfees = doctorfees;
        
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDoctorname() {
        return doctorname;
    }

    public void setDoctorname(String doctorname) {
        this.doctorname = doctorname;
    }

    public String getSpecialisation() {
        return specialisation;
    }

    public void setSpecialisation(String specialisation) {
        this.specialisation = specialisation;
    }

    public Integer getRoomno() {
        return roomno;
    }

    public void setRoomno(Integer roomno) {
        this.roomno = roomno;
    }

    public Float getDoctorfees() {
        return doctorfees;
    }

    public void setDoctorfees(Float doctorfees) {
        this.doctorfees = doctorfees;
    }
}