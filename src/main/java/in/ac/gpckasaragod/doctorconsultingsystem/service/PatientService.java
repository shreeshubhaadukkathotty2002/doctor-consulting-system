/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.doctorconsultingsystem.service;

import java.util.List;

/**
 *
 * @author student
 */
public interface PatientService {
   public String savePatient(String patientname,String patientdesease,Integer doctorid);
   public PatientService readPatient(Integer id);
   public List<PatientService>getAllPatient();
   public String deletePatient(Integer id);
   public String updatePatient(Integer id,String patientname,String patientdesease,Integer doctorid);                                                                  
}
