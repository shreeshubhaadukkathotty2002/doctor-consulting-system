/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.doctorconsultingsystem.service.Impl;

import in.ac.gpckasaragod.doctorconsultingsystem.service.PatientService;
import in.ac.gpckasaragod.doctorconsultingsystem.ui.model.data.PatientDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public abstract class PatientServiceImpl extends ConnectionServiceImpl implements PatientService {

    @Override
    public String savePatient(String patientname, String patientdesease, Integer doctorid) {
        try{
            Connection connection = getConnection();
            Statement statement =connection.createStatement();
            String query = "INSERT INTO PATIENT_DETAILS(STRING PATIENT_NAME,STRING PATIENT_DESEASE,INTEGER DOCTOR_ID) VALUES" +"('"+patientname+"','"+patientdesease+"','"+doctorid+"')";
            System.out.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status !=1 ){
                return "save failed";
            }else{
                return "save successfully";
            }
        } catch (SQLException ex) {
            Logger.getLogger(PatientServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "save failed";
           
            }
        
       // throw new UnsupportedOperationException("Not supported yet.")// Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    @Override
    public PatientService readPatient(Integer id) {
        PatientService patient;
        patient =  null;
    try{
     Connection connection = getConnection();
     Statement statement = connection.createStatement();
     String query = "SELECT *FROM PATIENT_DETAILS WHERE ID=" + id;
     ResultSet resultSet = statement.executeQuery(query);
     while (resultSet.next()) {
         id = resultSet.getInt("ID");
         id = resultSet.getInt("DOCTOR_ID");
     }
     } catch (SQLException ex) {
         Logger.getLogger(PatientServiceImpl.class.getName()).log(Level.SEVERE,null, ex);
     }
         return patient;
    }   //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    

    @Override
    public List<PatientDetail> getAllPatient() {
        
        List<PatientDetail> patientdetailss = new ArrayList<>();
    try {
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM PATIENT_DETAILS";
        ResultSet resultSet=statement.executeQuery(query);
        while (resultSet.next()) {
            
            Integer id = resultSet.getInt("ID");
            String patientname = resultSet.getString("PATIENT_NAME");
            String patientdesease = resultSet.getString("PATIENT_DESEASE");
            Integer doctorid =resultSet.getInt("DOCTOR_ID");
            PatientDetail patientService = new PatientDetail(id,patientname,patientdesease,doctorid);
            patientdetailss.add(patientService);
        }
    } catch (SQLException ex) {
        Logger.getLogger(PatientServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
    }
     return patientdetailss;
    }
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody

    @Override
    public String deletePatient(Integer id) {
            try{
           Connection connection = getConnection();
           String query ="DELETE FROM PATIENT_DETAILS WHERE ID =?";
           PreparedStatement statement =connection.prepareStatement(query);
           statement.setInt(1, id);
           int delete = statement.executeUpdate();
           if(delete !=1)
             return "Deleted failed";
            else
              return "Deleted successfully";
     } catch (SQLException ex) {
        Logger.getLogger(PatientServiceImpl.class .getName()).log(Level.SEVERE, null,ex);
}
    return "Deleted failed";
  }

       // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody

    @Override
    public String updatePatient(Integer id, String patientname, String patientdesease, Integer doctorid) {
     try{
       Connection connection  = getConnection();
       Statement statement = connection.createStatement();
       String query = "UPDATE PATIENT_DETAILS SET PATIENT_NAME='" + patientname + "',PATIENT_DESEASE='"+ patientdesease + "',DOCTOR_ID='" + doctorid + "'WHERE ID=" +id;
       System.out.print(query);
       int update =statement.executeUpdate(query);
       if (update !=1) {
           return "Update failed";
       }else {
           return "Update successfully";
       }
    } catch (SQLException ex) {
         Logger.getLogger(PatientServiceImpl.class.getName()).log(Level.SEVERE,null, ex);
       
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
     return "Update failed";
    }

    

        
    

    
    

   

        

}
