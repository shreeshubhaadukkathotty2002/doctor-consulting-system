/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.doctorconsultingsystem.service;

import in.ac.gpckasaragod.doctorconsultingsystem.ui.DoctorDetailsForm;
import in.ac.gpckasaragod.doctorconsultingsystem.ui.model.data.DoctorDetail;
import java.util.List;

/**
 *
 * @author student
 */
public interface DoctorService {
    
    
    public String saveDoctorDetails(String doctorName,String specialisation,Integer roomNo,Float doctorFees);
       public DoctorDetail readDoctorDetails(Integer id);
       public List<DoctorDetail> getAllDoctorDetails();
       public String updateDoctorDetails(Integer id,String doctorName,String specialisation,Integer roomNo,Float doctorFees); 
       public String deleteDoctorDetails(Integer id);


   
}
       
     
    