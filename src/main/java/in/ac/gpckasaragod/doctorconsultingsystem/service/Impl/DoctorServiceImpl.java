/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.doctorconsultingsystem.service.Impl;

import in.ac.gpckasaragod.doctorconsultingsystem.service.DoctorService;
import in.ac.gpckasaragod.doctorconsultingsystem.ui.DoctorDetailsForm;
import in.ac.gpckasaragod.doctorconsultingsystem.ui.model.data.DoctorDetail;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class DoctorServiceImpl extends ConnectionServiceImpl implements DoctorService {

    @Override
    public String saveDoctorDetails(String doctorname, String specialisation, Integer roomno, Float doctorfees) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO DOCTOR_DETAILS (DOCTORNAME,SPECIALISATION,ROOMNO,DOCTORFEES) VALUES"
                    + "('" + doctorname + "','" + specialisation + "','" + roomno + "','" + doctorfees + "')";
            System.err.println("Query:" + query);
            int status = statement.executeUpdate(query);
            if (status != 1) {
                return "Save failed";

            } else {
                return "Save successfully";

            }

        } catch (SQLException ex) {
            Logger.getLogger(DoctorServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "Save failed";
        }
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public DoctorDetail readDoctorDetails(Integer id) {
        DoctorDetail doctordetails= null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM DOCTOR WHERE ID=" + id;
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                id = resultSet.getInt("ID");
                String doctorname = resultSet.getString("DOCTORNAME");
                String specialisation = resultSet.getString("SPECIALISATION");
                Integer roomno = resultSet.getInt("ROOMNO");
                Float doctorfees = resultSet.getFloat("DOCTORFEES");
                doctordetails = new DoctorDetail(id, doctorname, specialisation, roomno, doctorfees);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DoctorServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return doctordetails;
    }
        /**
         *
         * @return
         */
    
   
    @Override
    public List<DoctorDetail> getAllDoctorDetails() {

        List<DoctorDetail> doctordetailss = new ArrayList<>();
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM DOCTOR";
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Integer id = resultSet.getInt("ID");
                String doctorname = resultSet.getString("DOCTORNAME");
                String specialisation = resultSet.getString("SPECIALISATION");
                Integer roomno = resultSet.getInt("ROOMNO");
                Float doctorfees = resultSet.getFloat("DOCTORFEES");
                DoctorDetail doctordetail = new DoctorDetail(id, doctorname, specialisation, roomno, doctorfees);
                doctordetailss.add(doctordetail);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DoctorServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return doctordetailss;
    }

    @Override
    public String updateDoctorDetails(Integer id, String doctorname, String specialisation, Integer roomno, Float doctorfees){

        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE DOCTORSERVICE SET DOCTORNAME='" + doctorname + "',SPECIALISATION='" + specialisation + "',ROOMNO='" + roomno + "',DOCTORFEES='" + doctorfees + "' WHERE ID=" + id;
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if (update != 1) {
                return "Update failed";
            } else {
                return "Update successfully";
            }
        } catch (SQLException ex) {
            Logger.getLogger(DoctorServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "update failed";
        }
    
    }

    @Override
    public String deleteDoctorDetails(Integer id) {

        try {
            Connection connection = getConnection();
            String query = "DELETE FROM DOCTORSERVICE WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if (delete != 1) {
                return "Delete failed";
            } else {
                return "Deleted successfully";
            }
        } catch (SQLException ex) {
            Logger.getLogger(DoctorServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Delete failed";
    }

 
    
}
